package depend;

import org.testng.Assert;
import org.testng.annotations.Test;

public class crud {

	@Test(priority=1,groups={"smoke","regression"})
	public void create(){
		System.out.println("Create");
		
		Assert.assertTrue(true);
		
		// ur actual will always b fetched by selenium
		Assert.assertEquals("ABC", "ABC123","Create user screen username doesnt match");
		Assert.assertNotEquals("vb", "bb");
	}
	
	@Test(priority=2,dependsOnMethods="create",groups={"smoke","regression"})
	public void read(){
		System.out.println("Read");
	}
	
	@Test(groups={"smoke","regression"},description="TCID00456- this verifies the update user functionality",priority=3,dependsOnMethods="create")
	public void update(){
		System.out.println("Update");
	}
	
	@Test(groups="smoke",priority=4,dependsOnMethods="create")
	public void delete(){
		System.out.println("delete");
	}
	
}
