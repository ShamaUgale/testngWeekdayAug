package annotations;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class A1 {
	
	
	@BeforeClass
	public void beforeClass(){
		System.out.println("In beforeclasss - A1");
	}
	
	
	@BeforeMethod
	public void beforeMethod(){
		System.out.println("In beforeMethod...");
	}
	
	@AfterMethod
	public void afterMethod(){
		System.out.println("In afterMethod...");
	}

	@Test(groups={"smoke"})
	public void test1(){
		System.out.println("In test1 ..");
		// ur selenium code will be here for ur test 1
	}
	
	
	@Test(groups={"regression"})
	public void test2(){
		System.out.println("In test2 ..");
		// ur selenium code will be here for ur test 1
	}
	
	
	@Test(groups={"regression"})
	public void test3(){
		System.out.println("In test3 ..");
		// ur selenium code will be here for ur test 1
	}
	
	
	@Test(groups={"smoke"})
	public void loginTest(){
		System.out.println("In test4 ..");
		// ur selenium code will be here for ur test 1
	}
	
	
}
