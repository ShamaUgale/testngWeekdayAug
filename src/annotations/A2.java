package annotations;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class A2 {

	@BeforeSuite
	public void beforeSuite(){
		System.out.println("In before suite -- A2");
	}
	
	
	@BeforeTest
	public void beforeTest(){
		System.out.println("In before test -- A2");
	}
	
	@BeforeClass()
	public void beforeClass(){
		System.out.println("In beforeclasss - A2");
	}
	
	
	@Test(groups={"smoke","regression"})
	public void test5(){
		System.out.println("In test5 ..");
		// ur selenium code will be here for ur test 1
	}
	
	@Test(groups={"regression"})
	public void test6(){
		System.out.println("In test 6..");
		// ur selenium code will be here for ur test 1
	}
	
}
