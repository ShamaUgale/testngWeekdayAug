package parameterization;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class registrationTest {	
	
	@Test(dataProvider="getData",groups={"regression"})
	public void registerTest(String fname, String lname, String email, String phno){
		System.out.println("First name : "+ fname);
		System.out.println("Last name : "+ lname);
		System.out.println("Email : "+ email);
		System.out.println("Phone No : "+ phno);
		System.out.println("**********************************************");
	}	
	
	@DataProvider
	public Object[][] getData(){
	
		Object[][] data= new Object[5][4];
		
		data[0][0]="F1";
		data[0][1]="L1";
		data[0][2]="Email 1";
		data[0][3]="Phone 1";
		

		data[1][0]="F2";
		data[1][1]="L2";
		data[1][2]="Email 2";
		data[1][3]="Phone 2";
		
		data[2][0]="F3";
		data[2][1]="L3";
		data[2][2]="Email 3";
		data[2][3]="Phone 3";
		
		data[3][0]="F4";
		data[3][1]="L4";
		data[3][2]="Email 4";
		data[3][3]="Phone 4";
		
		data[4][0]="F5";
		data[4][1]="L5";
		data[4][2]="Email 5";
		data[4][3]="Phone 5";
		
		return data;
		
		
	}
}
